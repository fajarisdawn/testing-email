class LandingController < ApplicationController
  def index
  end

  def newsletter
  	send_newsletter(params[:email], params[:message])
  	redirect_to root_url
  end

  protected

  def send_newsletter(email, description)
  	Newsletter.weekly(email, description).deliver_now
  end
end

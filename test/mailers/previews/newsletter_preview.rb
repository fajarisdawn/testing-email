# Preview all emails at http://localhost:3000/rails/mailers/newsletter
class NewsletterPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/newsletter/mailer
  def mailer
    Newsletter.mailer
  end

  # Preview this email at http://localhost:3000/rails/mailers/newsletter/weekly
  def weekly
    Newsletter.weekly
  end

end
